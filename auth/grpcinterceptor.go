package auth

import (
	"context"
	"log"

	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"
	"hdfcbank.com/mobileapp/templatemicroservice/common"
)

func GetEnsureGRPCCredentials(inCtx context.Context) func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
	transportConfig, ok := inCtx.Value(common.TransportContextKey{}).(common.TransportConfig)
	var identityService, configService, sessionService string
	if !ok {
		log.Println("Couldn't find transport config")
	} else {
		identityService = transportConfig.IdentityService
		configService = transportConfig.ConfigService
		sessionService = transportConfig.SessionService
	}

	return func(inCtx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {

		md, ok := metadata.FromIncomingContext(inCtx)
		if !ok {
			return nil, status.Errorf(codes.InvalidArgument, "missing metadata")
		}
		if len(md["token"]) == 0 || len(md["dynamic_token"]) == 0 || len(md["session_key"]) == 0 {
			return nil, status.Errorf(codes.InvalidArgument, "missing metadata")
		}
		err := Validate(md["token"][0], md["dynamic_token"][0], md["session_key"][0], identityService, sessionService, configService)
		if err != nil {
			return nil, status.Errorf(codes.Unauthenticated, "invalid credentials")
		}

		// Continue execution of handler after ensuring a valid token.
		authKeys := AuthKeys{Token: md["token"][0], DynamicToken: md["dynamic_token"][0], SessionKey: md["session_key"][0]}
		var ctxKey common.ContextKey
		newCtx := context.WithValue(inCtx, ctxKey, authKeys)
		return handler(newCtx, req)
	}
}
