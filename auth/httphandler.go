package auth

import (
	"log"
	"net/http"
)

//Currently strings used for identityService etc, but later inject the grpcService clients, so calls can be made
func GetAuthMiddleware(identityService string, sessionService string, configService string) func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			//Check if auth headers are present
			jwtToken := r.Header.Get("token")
			dynamicToken := r.Header.Get("dynamic_token")
			sessionKey := r.Header.Get("session_key")
			if len(jwtToken) == 0 || len(dynamicToken) == 0 || len(sessionKey) == 0 {
				log.Println("Auth tokens not present")
				w.WriteHeader(http.StatusForbidden)
				w.Write([]byte("Authentication failed"))
				return
			}
			err := Validate(jwtToken, dynamicToken, sessionKey, identityService, sessionService, configService)
			if err != nil {
				log.Println("Auth validation failed")
				w.WriteHeader(http.StatusForbidden)
				w.Write([]byte("Authentication failed"))
			}

			// Call the next handler, which can be another middleware in the chain, or the final handler.
			next.ServeHTTP(w, r)
		})
	}
}

/*
func AuthMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		//Check if auth headers are present
		jwtToken := r.Header.Get("token")
		dynamicToken := r.Header.Get("dynamic_token")
		sessionKey := r.Header.Get("session_key")
		if len(jwtToken) == 0 || len(dynamicToken) == 0 || len(sessionKey) == 0 {
			fmt.Println("Auth tokens not present")
			w.WriteHeader(http.StatusForbidden)
			w.Write([]byte("Authentication failed"))
			return
		}
		err := Validate(jwtToken, dynamicToken, sessionKey)
		if err != nil {
			fmt.Println("Auth validation failed")
			w.WriteHeader(http.StatusForbidden)
			w.Write([]byte("Authentication failed"))
		}

		// Call the next handler, which can be another middleware in the chain, or the final handler.
		next.ServeHTTP(w, r)
	})
}
*/
