package auth

import (
	"context"
	"net/http"

	"hdfcbank.com/mobileapp/templatemicroservice/common"
)

type AuthKeys struct {
	Token        string
	DynamicToken string
	SessionKey   string
}

func PutAuthKeysInCtx(ctx context.Context, r *http.Request) context.Context {
	var ctxKey common.ContextKey
	if ctx == nil {
		ctx = context.Background()
	}
	authKeys := AuthKeys{
		Token:        r.Header.Get("token"),
		DynamicToken: r.Header.Get("dynamic_token"),
		SessionKey:   r.Header.Get("session_key"),
	}
	return context.WithValue(ctx, ctxKey, authKeys)

}
