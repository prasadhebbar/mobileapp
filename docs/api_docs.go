// Package classification microservices.
//
// Documentation of template microservices API.
//
//     Schemes: http
//     BasePath: /
//     Version: 1.0.0
//     Host: some-url.com
//
//     Consumes:
//     - application/json
//
//     Produces:
//     - application/json
//
//     Security:
//     - basic
//
//    SecurityDefinitions:
//    basic:
//      type: basic
//
// swagger:meta

package docs

import (
	"hdfcbank.com/mobileapp/templatemicroservice/usersvcendpoint"
)

// CreateUser creates a new user
// responses:
//   200: CreateUserResponse

// CreateUser takes email id and password, and creates a new user id, and inserts it to the database
// swagger:response CreateUserResponse
type CreateUserResponseWrapper struct {
	// in:body
	Body usersvcendpoint.CreateUserResponse
}

// swagger:parameters idOfCreateUserEndpoint
type CreateUserParamsWrapper struct {
	// This text will appear as description of your request body.
	// in:body
	Body usersvcendpoint.CreateUserRequest
}
