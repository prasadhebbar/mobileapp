module hdfcbank.com/mobileapp

go 1.16

require (
	github.com/DATA-DOG/go-sqlmock v1.5.0
	github.com/go-kit/kit v0.12.0
	github.com/google/uuid v1.3.0
	github.com/gorilla/mux v1.8.0
	github.com/lib/pq v1.10.3
	github.com/stretchr/testify v1.7.0
	go.uber.org/zap v1.19.1
	google.golang.org/grpc v1.40.0
	google.golang.org/protobuf v1.27.1
)
