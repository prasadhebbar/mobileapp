package retcodes

import (
	"errors"
)

/*
System wide return codes and error messages accross all microservices maintained here.
Once Config microservice is up, this will be pulled from the Config microservice, and not
hardcoded in a file, so on-the-fly changes can be made to return messages.

Also messages are maintained for multiple languages
*/

type retInfo struct {
	errCode int
	message map[string]string
	err     error
}

type RetInfoInLang struct {
	ErrCode int    `json:"err_code"`
	Message string `json:"message"`
}

var retCodeMap = map[string]retInfo{
	ErrUserIDNotFound.Error():           userIDNotFound,
	ErrUserIDOrEmailNotProvided.Error(): userIDOrEmailNotProvided,
	ErrUserAlreadyExists.Error():        userAlreadyExists,
	ErrHttpReqNotFound.Error():          httpReqNotFound,
	ErrInvalidTokens.Error():            invalidTokens,
}
var successCode = retInfo{errCode: 0, message: map[string]string{"en": "Success"}, err: nil}
var unknownError = retInfo{errCode: -1, message: map[string]string{"en": "Error occured. Please try again in a couple minutes."}}

var ErrUserIDNotFound = errors.New("user not found")
var userIDNotFound = retInfo{errCode: 1, message: map[string]string{"en": "User id not found"}, err: ErrUserIDNotFound}

var ErrUserIDOrEmailNotProvided = errors.New("user or email id not provided")
var userIDOrEmailNotProvided = retInfo{errCode: 2, message: map[string]string{"en": "Both User id and Email cannot be blank"}, err: ErrUserIDOrEmailNotProvided}

var ErrUserAlreadyExists = errors.New("user already exists")
var userAlreadyExists = retInfo{errCode: 3, message: map[string]string{"en": "User already exists with this email id"}, err: ErrUserAlreadyExists}

var ErrHttpReqNotFound = errors.New("auth keys not found in context")
var httpReqNotFound = retInfo{errCode: 4, message: map[string]string{"en": "Invalid authentication. Please login again."}, err: ErrHttpReqNotFound}

var ErrInvalidTokens = errors.New("invalid or blank auth keys")
var invalidTokens = retInfo{errCode: 4, message: map[string]string{"en": "Invalid authentication. Please login again."}, err: ErrHttpReqNotFound}

func GetRetCode(err error, lang string) (RetInfoInLang, error) {
	var matchedRetInfo retInfo
	if err == nil {
		matchedRetInfo = successCode
	} else {
		if retCode, ok := retCodeMap[err.Error()]; ok {
			matchedRetInfo = retCode
		} else { //If non handled error then return it
			return RetInfoInLang{}, err
		}
	}

	if msg, ok := matchedRetInfo.message[lang]; ok {
		return RetInfoInLang{ErrCode: matchedRetInfo.errCode, Message: msg}, nil
	} else if msg, ok := matchedRetInfo.message["en"]; ok {
		return RetInfoInLang{ErrCode: matchedRetInfo.errCode, Message: msg}, nil
	}

	return RetInfoInLang{ErrCode: matchedRetInfo.errCode, Message: "No valid response in selected language"}, nil
}
