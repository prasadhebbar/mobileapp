package common

//ServiceConfig is used to inject configuration parameters to the service
type ServiceConfig struct {
	TemplateCalledMicroserviceDeadline int
}
