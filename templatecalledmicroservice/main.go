package main

import (
	"context"
	"database/sql"
	"flag"
	"fmt"
	"log"
	"net"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	_ "github.com/lib/pq"
	"go.uber.org/zap"
	"google.golang.org/grpc"
	"hdfcbank.com/mobileapp/auth"
	_ "hdfcbank.com/mobileapp/docs"
	"hdfcbank.com/mobileapp/pb"
	"hdfcbank.com/mobileapp/templatecalledmicroservice/common"
	"hdfcbank.com/mobileapp/templatecalledmicroservice/middleware"
	"hdfcbank.com/mobileapp/templatecalledmicroservice/repo"
	"hdfcbank.com/mobileapp/templatecalledmicroservice/service"
	"hdfcbank.com/mobileapp/templatecalledmicroservice/transport"
	"hdfcbank.com/mobileapp/templatecalledmicroservice/usersvcendpoint"
)

func main() {
	var logger *zap.Logger
	var err error
	{
		logger, err = zap.NewProduction()
		if err != nil {
			log.Fatal("Unable to initiate logging... aborting")
		}
		defer logger.Sync()
	}

	logger.Info("Service started")
	defer logger.Info("Service ended")

	httpAddr := os.Getenv("HTTP_ADDR") //host:port
	if len(httpAddr) == 0 {
		httpAddr = ":8080"
	}
	dbName := os.Getenv("DB_NAME")
	if len(dbName) == 0 {
		logger.Fatal("No database name provided")
	}

	dbUserName := os.Getenv("DB_USERNAME")
	if len(dbUserName) == 0 {
		logger.Fatal("No database user name provided")
	}

	dbPassword := os.Getenv("DB_PASSWORD")
	if len(dbPassword) == 0 {
		logger.Fatal("No database password provided")
	}

	dbAddr := os.Getenv("DB_ADDR") //host:port
	if len(dbAddr) == 0 {
		dbAddr = "localhost:5432"
	}

	grpcAddr := os.Getenv("GRPC_ADDR")
	if len(grpcAddr) == 0 {
		grpcAddr = ":9090"
	}

	dbFlags := os.Getenv("DB_FLAGS") //flag1=val1&flag2=val2 etc

	dbSource := fmt.Sprintf("postgresql://%s:%s@%s/%s?%s", dbUserName, dbPassword, dbAddr, dbName, dbFlags)

	var db *sql.DB
	{
		var err error

		db, err = sql.Open("postgres", dbSource)
		if err != nil {
			logger.Error("Unable to open database", zap.String("error", err.Error()))
			os.Exit(-1)
		}

	}
	defer db.Close()

	flag.Parse()

	//Error channel
	errChan := make(chan error)

	stopChan := make(chan os.Signal)
	// bind OS events to the signal channel
	signal.Notify(stopChan, syscall.SIGTERM, syscall.SIGINT)

	serviceConfig := common.ServiceConfig{}

	var srv service.UserService
	var repository repo.Repository

	{
		repository = repo.NewRepo(db, logger)

		srv = service.NewService(repository, logger, serviceConfig)
	}

	srv = middleware.LoggingMiddleware{Logger: logger, Next: srv}
	endpoints := usersvcendpoint.MakeEndpoints(srv)

	//Inject Identity, Config and session grpc clients. Currently only strings injected
	var transportContextKey common.TransportContextKey
	ctx := context.WithValue(context.Background(), transportContextKey, common.TransportConfig{IdentityService: "identity_service", SessionService: "session_service", ConfigService: "config_service"})

	//Start HTTP server
	go func() {
		logger.Info("Server started", zap.String("port", httpAddr))
		handler := transport.NewHTTPServer(ctx, endpoints)
		errChan <- http.ListenAndServe(httpAddr, handler)
	}()

	//Start the GRPC server, ok to not have grpcUserServiceClient initialized as it is not used in the gRPC server
	var baseServer *grpc.Server
	go func() {
		grpcServer := transport.NewGRPCServer(endpoints)
		// The gRPC listener mounts the Go kit gRPC server we created.
		grpcListener, err := net.Listen("tcp", grpcAddr)
		if err != nil {
			logger.Fatal("gRPC Server", zap.String("err", err.Error()))
		}
		logger.Info("gRPC Server started", zap.String("port", grpcAddr))

		opts := []grpc.ServerOption{
			//Auth interceptor for each grpc call, injecting context
			grpc.UnaryInterceptor(auth.GetEnsureGRPCCredentials(ctx)),
		}
		baseServer = grpc.NewServer(opts...)
		pb.RegisterUserServiceServer(baseServer, grpcServer)
		errChan <- baseServer.Serve(grpcListener)
	}()

	// block until either OS signal, or server fatal error
	select {
	case err := <-errChan:
		logger.Fatal("Error occured", zap.String("err", err.Error()))
	case <-stopChan:
		logger.Info("Shutting down server")
		baseServer.GracefulStop()
	}

}
