package middleware

import (
	"context"
	"time"

	"go.uber.org/zap"
	"hdfcbank.com/mobileapp/templatecalledmicroservice/service"
)

//Example of Middleware. This intercepts the service call and calculates time taken
//This is only an example and need not be used

type LoggingMiddleware struct {
	Logger *zap.Logger
	Next   service.UserService
}

func (mw LoggingMiddleware) CreateUser(ctx context.Context, email string, password string) (output string, err error) {
	defer func(begin time.Time) {
		var errStr string

		if err != nil {
			errStr = err.Error()
		}
		mw.Logger.Info(
			"CreateUser called",
			zap.String("method", "CreateUser"),
			zap.String("input", email+"|"+password),
			zap.String("output", output),
			zap.String("err", errStr),
			zap.Int("took", int(time.Since(begin).Nanoseconds())),
		)
	}(time.Now())

	output, err = mw.Next.CreateUser(ctx, email, password)
	return
}

func (mw LoggingMiddleware) GetUser(ctx context.Context, id string) (output string, err error) {
	defer func(begin time.Time) {
		var errStr string

		if err != nil {
			errStr = err.Error()
		}
		mw.Logger.Info(
			"GetUser called",
			zap.String("method", "GetUser"),
			zap.String("input", id),
			zap.String("output", output),
			zap.String("err", errStr),
			zap.Int("took", int(time.Since(begin).Nanoseconds())),
		)
	}(time.Now())

	output, err = mw.Next.GetUser(ctx, id)
	return
}

func (mw LoggingMiddleware) Search(ctx context.Context, search string) (output string, err error) {
	defer func(begin time.Time) {
		var errStr string

		if err != nil {
			errStr = err.Error()
		}
		mw.Logger.Info(
			"Search called",
			zap.String("method", "Search"),
			zap.String("input", search),
			zap.String("output", output),
			zap.String("err", errStr),
			zap.Int("took", int(time.Since(begin).Nanoseconds())),
		)
	}(time.Now())

	output, err = mw.Next.Search(ctx, search)
	return
}
