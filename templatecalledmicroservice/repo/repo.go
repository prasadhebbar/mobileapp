package repo

import (
	"context"
	"database/sql"
	"strings"

	"go.uber.org/zap"
	"hdfcbank.com/mobileapp/retcodes"
)

type User struct {
	ID       string `json:"id,omitempty"`
	Email    string `json:"email"`
	Password string `json:"password"`
}

type Repository interface {
	CreateUser(ctx context.Context, user User) error
	GetUser(ctx context.Context, id string) (string, error)
}

type Repo struct {
	DB     *sql.DB
	Logger *zap.Logger
}

func NewRepo(db *sql.DB, logger *zap.Logger) Repository {
	return &Repo{
		DB:     db,
		Logger: logger,
	}
}

//Todo Have SQLs as prepared statements
func (repo *Repo) CreateUser(ctx context.Context, user User) error {
	sql := "INSERT INTO users (id, email, password) VALUES ($1, $2, $3)"

	if user.Email == "" || user.Password == "" {
		return retcodes.ErrUserIDOrEmailNotProvided
	}

	_, err := repo.DB.ExecContext(ctx, sql, user.ID, user.Email, user.Password)
	if err != nil {
		if strings.Contains(err.Error(), "duplicate key value violates unique constraint") {
			return retcodes.ErrUserAlreadyExists
		}
		return err
	}
	return nil
}

func (repo *Repo) GetUser(ctx context.Context, id string) (string, error) {
	var email string
	err := repo.DB.QueryRowContext(ctx, "SELECT email FROM users WHERE id=$1", id).Scan(&email)
	if err != nil {
		if err.Error() == "sql: no rows in result set" {
			return "", retcodes.ErrUserIDNotFound
		}
		repo.Logger.Error("repo.GetUser", zap.String("error", err.Error()))
		return "", err

	}

	return email, nil
}
