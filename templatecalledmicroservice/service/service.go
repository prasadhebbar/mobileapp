package service

import (
	"context"
	"strconv"
	"time"

	"go.uber.org/zap"
	"hdfcbank.com/mobileapp/auth"
	"hdfcbank.com/mobileapp/retcodes"
	"hdfcbank.com/mobileapp/templatecalledmicroservice/common"
	"hdfcbank.com/mobileapp/templatecalledmicroservice/repo"
)

// UserService provides user operations.
type UserService interface {
	CreateUser(ctx context.Context, email string, password string) (string, error)
	GetUser(ctx context.Context, id string) (string, error)
	Search(ctx context.Context, search string) (string, error)
}

type UserServiceImpl struct {
	Repository repo.Repository
	Logger     *zap.Logger
	ServConfig common.ServiceConfig
}

func NewService(rep repo.Repository, logger *zap.Logger, serviceConfig common.ServiceConfig) UserService {
	return &UserServiceImpl{
		Repository: rep,
		Logger:     logger,
		ServConfig: serviceConfig,
	}
}

func (s UserServiceImpl) CreateUser(ctx context.Context, email string, password string) (string, error) {
	id := strconv.FormatInt(time.Now().Unix(), 10)
	user := repo.User{
		ID:       id,
		Email:    id + email, //For dummy purposes, to ensure there is a different id per request
		Password: password,
	}

	if err := s.Repository.CreateUser(ctx, user); err != nil {
		s.Logger.Error("CreateUser", zap.String("error", err.Error()))
		return "", err
	}

	s.Logger.Info("CreateUser", zap.String("New user id", id))

	return id, nil
}

func (s UserServiceImpl) GetUser(ctx context.Context, id string) (string, error) {

	//Example of getting header values from request for authentication & authorization
	var ctxKey common.ContextKey
	authkeys, ok := ctx.Value(ctxKey).(auth.AuthKeys)

	if !ok {
		return "", retcodes.ErrHttpReqNotFound
	}

	if len(authkeys.Token) == 0 || len(authkeys.DynamicToken) == 0 || len(authkeys.SessionKey) == 0 {
		return "", retcodes.ErrInvalidTokens
	}
	//User authorization based on tokens can be done next. Authentication is already handled

	email, err := s.Repository.GetUser(ctx, id)

	if err != nil {
		s.Logger.Error("GetUser", zap.String("error", err.Error()))
		return "", err
	}

	s.Logger.Info("GetUser", zap.String("User id", id))

	return email, nil
}

//Example of calling another service through HTTP
/*
func (s UserServiceImpl) Search(ctx context.Context, query string) (string, error) {
	jsonData := fmt.Sprintf(`{"id": "%s"}`, query)

	//DNS should be used here instead of ip addr for load balancing
	req, err := http.NewRequest("POST", "http://localhost:8080/get_user", bytes.NewBuffer([]byte(jsonData)))
	if err != nil {
		return "", err
	}
	req.Header.Set("Content-Type", "application/json; charset=UTF-8")

	var results string
	newCtx, cancel := context.WithTimeout(ctx, time.Second*4) //Set appropriate timeout
	defer cancel()
	err = httpDo(newCtx, req, func(resp *http.Response, err error) error {
		if err != nil {
			return err
		}
		defer resp.Body.Close()

		data, readErr := ioutil.ReadAll(resp.Body)
		if readErr != nil {
			s.Logger.Error("Error reading service response", zap.String("error", err.Error()))
			return readErr
		}

		results = string(data)
		return nil
	})

	return results, err
}
*/

func (s UserServiceImpl) Search(ctx context.Context, query string) (string, error) {

	//time.Sleep(5 * time.Second)
	email, err := s.Repository.GetUser(ctx, query)

	if err != nil {
		s.Logger.Error("GetUser", zap.String("error", err.Error()))
		return "", err
	}

	s.Logger.Info("CreateUser", zap.String("New user id", query))

	return email, nil

}
