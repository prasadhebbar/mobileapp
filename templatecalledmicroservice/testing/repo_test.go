package main

import (
	"context"
	"database/sql"
	"log"
	"testing"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"go.uber.org/zap"
	"hdfcbank.com/mobileapp/templatecalledmicroservice/repo"
)

var user = repo.User{
	ID:       uuid.New().String(),
	Email:    "test@testing.com",
	Password: "password",
}

func NewMock() (*sql.DB, sqlmock.Sqlmock) {
	db, mock, err := sqlmock.New()
	if err != nil {
		log.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}

	return db, mock
}

func TestRepoGetUser(t *testing.T) {
	db, mock := NewMock()
	logger, err := zap.NewProduction()
	if err != nil {
		log.Fatal("Unable to initiate logging... aborting")
	}
	defer logger.Sync()
	repo := &repo.Repo{db, logger}
	defer func() {
		repo.DB.Close()
	}()

	query := "SELECT email FROM users WHERE id=\\$1"

	rows := sqlmock.NewRows([]string{"email"}).
		AddRow(user.Email)

	mock.ExpectQuery(query).WithArgs(user.ID).WillReturnRows(rows)

	user, err := repo.GetUser(context.Background(), user.ID)
	assert.NotNil(t, user)
	assert.NoError(t, err)
}

func TestRepoGetUserNoRows(t *testing.T) {
	db, mock := NewMock()
	logger, err := zap.NewProduction()
	if err != nil {
		log.Fatal("Unable to initiate logging... aborting")
	}
	defer logger.Sync()
	repo := &repo.Repo{db, logger}
	defer func() {
		repo.DB.Close()
	}()

	query := "SELECT email FROM users WHERE id=\\$1"

	rows := sqlmock.NewRows([]string{"email"})

	mock.ExpectQuery(query).WithArgs(user.ID).WillReturnRows(rows)

	user, err := repo.GetUser(context.Background(), user.ID)
	assert.NotNil(t, user)
	assert.NoError(t, err)
}

func TestRepoCreateUser(t *testing.T) {
	db, mock := NewMock()
	logger, err := zap.NewProduction()
	if err != nil {
		log.Fatal("Unable to initiate logging... aborting")
	}
	defer logger.Sync()
	repo := &repo.Repo{db, logger}
	defer func() {
		repo.DB.Close()
	}()

	query := "INSERT INTO users (id, email, password) VALUES (\\$1, \\$2, \\$3)"

	//prep := mock.ExpectPrepare(query)
	//prep.ExpectExec().WithArgs(user.ID, user.Email, user.Password).WillReturnResult(sqlmock.NewResult(0, 1))

	mock.ExpectExec(query).WithArgs(user.ID, user.Email, user.Password).WillReturnResult(sqlmock.NewResult(0, 1))

	err = repo.CreateUser(context.Background(), user)
	assert.NoError(t, err)

}

func TestRepoCreateUserError(t *testing.T) {
	db, mock := NewMock()
	logger, err := zap.NewProduction()
	if err != nil {
		log.Fatal("Unable to initiate logging... aborting")
	}
	defer logger.Sync()
	repo := &repo.Repo{db, logger}
	defer func() {
		repo.DB.Close()
	}()

	query := "INSERT INTO users (id, email, password) VALUES (\\$1, \\$2, \\$3)"

	//prep := mock.ExpectPrepare(query)
	//prep.ExpectExec().WithArgs(user.ID, user.Email, user.Password).WillReturnResult(sqlmock.NewResult(0, 0))

	mock.ExpectExec(query).WithArgs(user.ID, user.Email, user.Password).WillReturnResult(sqlmock.NewResult(0, 0))

	err = repo.CreateUser(context.Background(), user)
	assert.NoError(t, err)

}
