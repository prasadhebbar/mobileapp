package transport

import (
	"context"
	"encoding/json"
	"log"
	"net/http"

	httptransport "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"
	"hdfcbank.com/mobileapp/auth"
	"hdfcbank.com/mobileapp/templatecalledmicroservice/common"
	"hdfcbank.com/mobileapp/templatecalledmicroservice/usersvcendpoint"
)

func encodeResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	return json.NewEncoder(w).Encode(response)
}

func decodeUserReq(ctx context.Context, r *http.Request) (interface{}, error) {
	var req usersvcendpoint.CreateUserRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		return nil, err
	}
	return req, nil
}

func decodeEmailReq(ctx context.Context, r *http.Request) (interface{}, error) {
	var req usersvcendpoint.GetUserRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		return nil, err
	}
	return req, nil
}

func decodeSearchReq(ctx context.Context, r *http.Request) (interface{}, error) {
	var req usersvcendpoint.GetSearchRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		return nil, err
	}
	return req, nil
}

func NewHTTPServer(ctx context.Context, endpoints usersvcendpoint.Endpoints) http.Handler {
	transportConfig, ok := ctx.Value(common.TransportContextKey{}).(common.TransportConfig)
	var identityService, configService, sessionService string
	if !ok {
		log.Println("Couldn't find transport config")
	} else {
		identityService = transportConfig.IdentityService
		configService = transportConfig.ConfigService
		sessionService = transportConfig.SessionService
	}

	r := mux.NewRouter()
	r.Use(auth.GetAuthMiddleware(identityService, configService, sessionService))

	serverOptions := []httptransport.ServerOption{
		httptransport.ServerBefore(auth.PutAuthKeysInCtx),
	}

	r.Methods("POST").Path("/user").Handler(httptransport.NewServer(
		endpoints.CreateUser,
		decodeUserReq,
		encodeResponse,
		serverOptions...,
	))

	r.Methods("POST").Path("/get_user").Handler(httptransport.NewServer(
		endpoints.GetUser,
		decodeEmailReq,
		encodeResponse,
		serverOptions...,
	))

	r.Methods("GET").Path("/search").Handler(httptransport.NewServer(
		endpoints.Search,
		decodeSearchReq,
		encodeResponse,
		serverOptions...,
	))

	return r

}
