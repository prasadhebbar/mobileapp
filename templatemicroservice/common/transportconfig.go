package common

//TransportConfig is used to inject configuration parameters to transport layer
type TransportConfig struct {
	IdentityService string //Later these to be changed to grpc cients
	ConfigService   string
	SessionService  string
}
