package main

import (
	"context"
	"database/sql"
	"flag"
	"fmt"
	"log"
	"net"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"syscall"

	_ "github.com/lib/pq"
	"go.uber.org/zap"
	"google.golang.org/grpc"
	"hdfcbank.com/mobileapp/auth"
	_ "hdfcbank.com/mobileapp/docs"
	"hdfcbank.com/mobileapp/pb"
	"hdfcbank.com/mobileapp/templatemicroservice/common"
	"hdfcbank.com/mobileapp/templatemicroservice/middleware"
	"hdfcbank.com/mobileapp/templatemicroservice/repo"
	"hdfcbank.com/mobileapp/templatemicroservice/service"
	"hdfcbank.com/mobileapp/templatemicroservice/transport"
	"hdfcbank.com/mobileapp/templatemicroservice/usersvcendpoint"
)

func main() {
	var logger *zap.Logger
	var err error
	{
		logger, err = zap.NewProduction()
		if err != nil {
			log.Fatal("Unable to initiate logging... aborting")
		}
		defer logger.Sync()
	}

	logger.Info("Service started")
	defer logger.Info("Service ended")

	httpAddr := os.Getenv("HTTP_ADDR") //host:port
	if len(httpAddr) == 0 {
		httpAddr = ":8080"
	}
	dbName := os.Getenv("DB_NAME")
	if len(dbName) == 0 {
		logger.Fatal("No database name provided")
	}

	dbUserName := os.Getenv("DB_USERNAME")
	if len(dbUserName) == 0 {
		logger.Fatal("No database user name provided")
	}

	dbPassword := os.Getenv("DB_PASSWORD")
	if len(dbPassword) == 0 {
		logger.Fatal("No database password provided")
	}

	dbAddr := os.Getenv("DB_ADDR") //host:port
	if len(dbAddr) == 0 {
		dbAddr = "localhost:5432"
	}

	grpcAddr := os.Getenv("GRPC_ADDR")
	if len(grpcAddr) == 0 {
		grpcAddr = ":9090"
	}

	dbFlags := os.Getenv("DB_FLAGS") //flag1=val1&flag2=val2 etc

	calledMicroserviceGrpcAddr := os.Getenv("TEMPLATECALLEDMICROSERVICE_GRPCADDR")
	if len(calledMicroserviceGrpcAddr) == 0 {
		logger.Fatal("No address for templatecalledmicroservice provided")
	}

	calledMicroserviceDeadline := os.Getenv("TEMPLATECALLEDMICROSERVICE_DEADLINE")
	if len(calledMicroserviceDeadline) == 0 {
		calledMicroserviceDeadline = "4"
	}
	calledMicroserviceDeadlineInt, err := strconv.Atoi(calledMicroserviceDeadline)
	if err != nil {
		logger.Info("Invalid non-int value for TEMPLATECALLEDMICROSERVICE_DEADLINE provided: " + calledMicroserviceDeadline)
		calledMicroserviceDeadlineInt = 4
	}

	dbSource := fmt.Sprintf("postgresql://%s:%s@%s/%s?%s", dbUserName, dbPassword, dbAddr, dbName, dbFlags)

	var db *sql.DB
	{
		var err error

		db, err = sql.Open("postgres", dbSource)
		if err != nil {
			logger.Error("Unable to open database", zap.String("error", err.Error()))
			os.Exit(-1)
		}

	}
	defer db.Close()

	flag.Parse()

	//Error channel
	errChan := make(chan error)

	stopChan := make(chan os.Signal)
	// bind OS events to the signal channel
	signal.Notify(stopChan, syscall.SIGTERM, syscall.SIGINT)

	//Demo of a grpc client connection into another microservice
	//Open a client connection to get user service and inject in into service
	// Set up a connection to the server.
	var grpcUserServiceClient pb.UserServiceClient
	grpcConn, err := grpc.Dial(calledMicroserviceGrpcAddr, grpc.WithInsecure()) //This would in reality be a completely different microservice but for demo pointing to clone of the same with search gRPC implemented
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer grpcConn.Close()
	grpcUserServiceClient = pb.NewUserServiceClient(grpcConn)

	serviceConfig := common.ServiceConfig{
		TemplateCalledMicroserviceDeadline: calledMicroserviceDeadlineInt,
	}

	var srv service.UserService
	var repository repo.Repository

	{
		repository = repo.NewRepo(db, logger)

		srv = service.NewService(repository, logger, grpcUserServiceClient, serviceConfig)
	}

	srv = middleware.LoggingMiddleware{Logger: logger, Next: srv}
	endpoints := usersvcendpoint.MakeEndpoints(srv)

	//Inject Identity, Config and session grpc clients. Currently only strings injected
	var transportContextKey common.TransportContextKey
	ctx := context.WithValue(context.Background(), transportContextKey, common.TransportConfig{IdentityService: "identity_service", SessionService: "session_service", ConfigService: "config_service"})

	//Start HTTP server
	go func() {
		logger.Info("Server started", zap.String("port", httpAddr))
		handler := transport.NewHTTPServer(ctx, endpoints)
		errChan <- http.ListenAndServe(httpAddr, handler)
	}()

	//Start the GRPC server, ok to not have grpcUserServiceClient initialized as it is not used in the gRPC server
	var baseServer *grpc.Server
	go func() {
		grpcServer := transport.NewGRPCServer(endpoints)
		// The gRPC listener mounts the Go kit gRPC server we created.
		grpcListener, err := net.Listen("tcp", grpcAddr)
		if err != nil {
			logger.Fatal("gRPC Server", zap.String("err", err.Error()))
		}
		logger.Info("gRPC Server started", zap.String("port", grpcAddr))

		opts := []grpc.ServerOption{
			//Auth interceptor for each grpc call, injecting context
			grpc.UnaryInterceptor(auth.GetEnsureGRPCCredentials(ctx)),
		}
		baseServer = grpc.NewServer(opts...)
		pb.RegisterUserServiceServer(baseServer, grpcServer)
		errChan <- baseServer.Serve(grpcListener)
	}()

	// block until either OS signal, or server fatal error
	select {
	case err := <-errChan:
		logger.Fatal("Error occured", zap.String("err", err.Error()))
	case <-stopChan:
		logger.Info("Shutting down server")
		baseServer.GracefulStop()
	}

}
