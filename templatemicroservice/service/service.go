package service

import (
	"context"
	"fmt"
	"strconv"
	"time"

	"go.uber.org/zap"
	"google.golang.org/grpc/metadata"
	"hdfcbank.com/mobileapp/auth"
	"hdfcbank.com/mobileapp/pb"
	"hdfcbank.com/mobileapp/retcodes"
	"hdfcbank.com/mobileapp/templatemicroservice/common"
	"hdfcbank.com/mobileapp/templatemicroservice/repo"
)

// UserService provides user operations.
type UserService interface {
	CreateUser(ctx context.Context, email string, password string) (string, error)
	GetUser(ctx context.Context, id string) (string, error)
	Search(ctx context.Context, search string) (string, error)
}

type UserServiceImpl struct {
	Repository            repo.Repository
	Logger                *zap.Logger
	UserServiceGRPCClient pb.UserServiceClient //For making downstream GRPC requests
	ServConfig            common.ServiceConfig
}

func NewService(rep repo.Repository, logger *zap.Logger, userServiceGRPCClient pb.UserServiceClient, serviceConfig common.ServiceConfig) UserService {
	return &UserServiceImpl{
		Repository:            rep,
		Logger:                logger,
		UserServiceGRPCClient: userServiceGRPCClient,
		ServConfig:            serviceConfig,
	}
}

func (s UserServiceImpl) CreateUser(ctx context.Context, email string, password string) (string, error) {
	id := strconv.FormatInt(time.Now().Unix(), 10)
	user := repo.User{
		ID:       id,
		Email:    id + email, //For dummy purposes, to ensure there is a different id per request
		Password: password,
	}

	if err := s.Repository.CreateUser(ctx, user); err != nil {
		s.Logger.Error("CreateUser", zap.String("error", err.Error()))
		return "", err
	}

	s.Logger.Info("CreateUser", zap.String("New user id", id))

	return id, nil
}

func (s UserServiceImpl) GetUser(ctx context.Context, id string) (string, error) {

	//Example of getting header values from request for authentication & authorization
	var ctxKey common.ContextKey
	authkeys, ok := ctx.Value(ctxKey).(auth.AuthKeys)

	if !ok {
		return "", retcodes.ErrHttpReqNotFound
	}

	if len(authkeys.Token) == 0 || len(authkeys.DynamicToken) == 0 || len(authkeys.SessionKey) == 0 {
		return "", retcodes.ErrInvalidTokens
	}
	//User authorization based on tokens can be done next. Authentication is already handled

	email, err := s.Repository.GetUser(ctx, id)

	if err != nil {
		s.Logger.Error("GetUser", zap.String("error", err.Error()))
		return "", err
	}

	s.Logger.Info("GetUser", zap.String("User id", id))

	return email, nil
}

//Example of calling another service through HTTP
/*
func (s UserServiceImpl) Search(ctx context.Context, query string) (string, error) {
	jsonData := fmt.Sprintf(`{"id": "%s"}`, query)

	//DNS should be used here instead of ip addr for load balancing
	req, err := http.NewRequest("POST", "http://localhost:8080/get_user", bytes.NewBuffer([]byte(jsonData)))
	if err != nil {
		return "", err
	}
	req.Header.Set("Content-Type", "application/json; charset=UTF-8")

	var results string
	newCtx, cancel := context.WithTimeout(ctx, time.Second*4) //Set appropriate timeout
	defer cancel()
	err = httpDo(newCtx, req, func(resp *http.Response, err error) error {
		if err != nil {
			return err
		}
		defer resp.Body.Close()

		data, readErr := ioutil.ReadAll(resp.Body)
		if readErr != nil {
			s.Logger.Error("Error reading service response", zap.String("error", err.Error()))
			return readErr
		}

		results = string(data)
		return nil
	})

	return results, err
}
*/

//Example of calling another service through gRPC
func (s UserServiceImpl) Search(ctx context.Context, query string) (string, error) {

	//Get the passed tokens and pass it to the called microservice
	var ctxKey common.ContextKey
	authkeys, ok := ctx.Value(ctxKey).(auth.AuthKeys)

	if !ok {
		return "", retcodes.ErrHttpReqNotFound
	}

	if len(authkeys.Token) == 0 || len(authkeys.DynamicToken) == 0 || len(authkeys.SessionKey) == 0 {
		return "", retcodes.ErrInvalidTokens
	}

	metadataCtx := metadata.AppendToOutgoingContext(ctx, "token", authkeys.Token, "dynamic_token", authkeys.DynamicToken, "session_key", authkeys.SessionKey)

	passedCtx, cancel := context.WithTimeout(metadataCtx, time.Duration(s.ServConfig.TemplateCalledMicroserviceDeadline)*time.Second) //This setup has to be through YAML
	defer cancel()

	if s.UserServiceGRPCClient == nil {
		return "", fmt.Errorf("no valid grpc client")
	}
	var results string
	r, err := s.UserServiceGRPCClient.Search(passedCtx, &pb.SearchRequest{Query: query})
	if err != nil {
		s.Logger.Error("Search", zap.String("err", err.Error()))
	} else {
		results = r.Response
	}

	return results, err

}
