package transport

import (
	"context"

	grpctransport "github.com/go-kit/kit/transport/grpc"

	"hdfcbank.com/mobileapp/pb"
	"hdfcbank.com/mobileapp/templatemicroservice/usersvcendpoint"
)

type grpcServer struct {
	createUser grpctransport.Handler
	getUser    grpctransport.Handler
	search     grpctransport.Handler
}

// NewGRPCServer makes a set of endpoints available as a gRPC UserServiceServer.
func NewGRPCServer(endpoints usersvcendpoint.Endpoints) pb.UserServiceServer {

	options := []grpctransport.ServerOption{}
	return &grpcServer{
		createUser: grpctransport.NewServer(
			endpoints.CreateUser,
			decodeGRPCCreateUserReq,
			encodeGRPCCreateUserResp,
			options...,
		),
		getUser: grpctransport.NewServer(
			endpoints.GetUser,
			decodeGRPCGetUserReq,
			encodeGRPCGetUserResp,
			options...,
		),
		search: grpctransport.NewServer(
			endpoints.Search,
			decodeGRPCSearchReq,
			encodeGRPCSearchResp,
			options...,
		),
	}
}

func (s *grpcServer) CreateUser(ctx context.Context, req *pb.CreateUserRequest) (*pb.CreateUserResponse, error) {
	_, rep, err := s.createUser.ServeGRPC(ctx, req)
	if err != nil {
		return nil, err
	}
	return rep.(*pb.CreateUserResponse), nil
}

func (s *grpcServer) GetUser(ctx context.Context, req *pb.GetUserRequest) (*pb.GetUserResponse, error) {
	_, rep, err := s.getUser.ServeGRPC(ctx, req)
	if err != nil {
		return nil, err
	}
	return rep.(*pb.GetUserResponse), nil
}

func (s *grpcServer) Search(ctx context.Context, req *pb.SearchRequest) (*pb.SearchResponse, error) {
	_, rep, err := s.search.ServeGRPC(ctx, req)
	if err != nil {
		return nil, err
	}
	return rep.(*pb.SearchResponse), nil
}

// decodeGRPCCreateUserReq is a transport/grpc.DecodeRequestFunc that converts a
// gRPC create user request to a user-domain create user request. Primarily useful in a server.
func decodeGRPCCreateUserReq(_ context.Context, grpcReq interface{}) (interface{}, error) {
	req := grpcReq.(*pb.CreateUserRequest)
	return usersvcendpoint.CreateUserRequest{Email: req.Email, Password: req.Password}, nil
}

// decodeGRPCGetUserReq is a transport/grpc.DecodeRequestFunc that converts a
// gRPC get user request to a user-domain get user request. Primarily useful in a server.
func decodeGRPCGetUserReq(_ context.Context, grpcReq interface{}) (interface{}, error) {
	req := grpcReq.(*pb.GetUserRequest)
	return usersvcendpoint.GetUserRequest{Id: req.Id}, nil
}

// decodeGRPCEmailReq is a transport/grpc.DecodeRequestFunc that converts a
// gRPC get user request to a user-domain get user request. Primarily useful in a server.
func decodeGRPCSearchReq(_ context.Context, grpcReq interface{}) (interface{}, error) {
	req := grpcReq.(*pb.SearchRequest)
	return usersvcendpoint.GetSearchRequest{Query: req.Query}, nil
}

// encodeGRPCCreateUserResp is a transport/grpc.EncodeResponseFunc that converts a
// user-domain create user  response to a gRPC create user reply. Primarily useful in a server.
func encodeGRPCCreateUserResp(_ context.Context, response interface{}) (interface{}, error) {
	resp := response.(usersvcendpoint.CreateUserResponse)
	return &pb.CreateUserResponse{Id: resp.Id, StatusCode: int32(resp.ReturnInfo.ErrCode), Msg: resp.ReturnInfo.Message}, nil
}

// encodeGRPCEmailResp is a transport/grpc.EncodeResponseFunc that converts a
// user-domain get user  response to a gRPC get user reply. Primarily useful in a server.
func encodeGRPCGetUserResp(_ context.Context, response interface{}) (interface{}, error) {
	resp := response.(usersvcendpoint.GetUserResponse)
	return &pb.GetUserResponse{Email: resp.Email, StatusCode: int32(resp.ReturnInfo.ErrCode), Msg: resp.ReturnInfo.Message}, nil
}

// encodeGRPCSearchResp is a transport/grpc.EncodeResponseFunc that converts a
// user-domain search  response to a gRPC search reply. Primarily useful in a server.
func encodeGRPCSearchResp(_ context.Context, response interface{}) (interface{}, error) {
	resp := response.(usersvcendpoint.GetSearchResponse)
	return &pb.SearchResponse{Response: resp.Response, StatusCode: int32(resp.ReturnInfo.ErrCode), Msg: resp.ReturnInfo.Message}, nil
}
