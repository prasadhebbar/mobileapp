package usersvcendpoint

import (
	"context"

	"github.com/go-kit/kit/endpoint"
	"hdfcbank.com/mobileapp/retcodes"
	"hdfcbank.com/mobileapp/templatemicroservice/service"
)

type Endpoints struct {
	CreateUser endpoint.Endpoint
	GetUser    endpoint.Endpoint
	Search     endpoint.Endpoint
}

func MakeEndpoints(s service.UserService) Endpoints {
	return Endpoints{
		CreateUser: makeCreateUserEndpoint(s),
		GetUser:    makeGetUserEndpoint(s),
		Search:     makeSearchEndpoint(s),
	}
}

func makeCreateUserEndpoint(s service.UserService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(CreateUserRequest)
		id, err := s.CreateUser(ctx, req.Email, req.Password)
		retInfo, retInfoErr := retcodes.GetRetCode(err, "en")
		if retInfoErr == nil {
			return CreateUserResponse{Id: id, ReturnInfo: retInfo}, nil
		}
		return CreateUserResponse{Id: id, ReturnInfo: retInfo}, retInfoErr
	}
}

func makeGetUserEndpoint(s service.UserService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(GetUserRequest)
		email, err := s.GetUser(ctx, req.Id)
		retInfo, retInfoErr := retcodes.GetRetCode(err, "en")
		if retInfoErr == nil {
			return GetUserResponse{Email: email, ReturnInfo: retInfo}, nil
		}
		return GetUserResponse{Email: email, ReturnInfo: retInfo}, retInfoErr
	}
}

func makeSearchEndpoint(s service.UserService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(GetSearchRequest)
		resp, err := s.Search(ctx, req.Query)

		retInfo, retInfoErr := retcodes.GetRetCode(err, "en")
		if retInfoErr == nil {
			return GetSearchResponse{Response: resp, ReturnInfo: retInfo}, nil
		}

		return GetSearchResponse{Response: resp, ReturnInfo: retInfo}, retInfoErr
	}
}

type (
	CreateUserRequest struct {
		Email    string `json:"email"`
		Password string `json:"password"`
	}
	CreateUserResponse struct {
		Id         string                 `json:"id"`
		ReturnInfo retcodes.RetInfoInLang `json:"return_info"`
	}

	GetUserRequest struct {
		Id string `json:"id"`
	}
	GetUserResponse struct {
		Email      string                 `json:"email"`
		ReturnInfo retcodes.RetInfoInLang `json:"return_info"`
	}

	GetSearchRequest struct {
		Query string `json:"query"`
	}
	GetSearchResponse struct {
		Response   string                 `json:"response"`
		ReturnInfo retcodes.RetInfoInLang `json:"return_info"`
	}
)
